# frozen_string_literal: true

require 'dotenv/load'
require 'byebug'
require 'net/imap'
require 'net/smtp'
require 'nokogiri'
require 'csv'
require 'prawn'
require_relative 'generate_declaration/imap'
require_relative 'generate_declaration/tracker'
require_relative 'generate_declaration/parser'
require_relative 'generate_declaration/generator'
require_relative 'generate_declaration/smtp'

module Reports
  # Must run before the main call to set up /tmp directory
  def self.create_tmp_dir
    dir = File.join('/tmp', ('a'..'z').to_a.shuffle.first(10).join)
    Dir.mkdir(dir) unless File.exists?(dir)
    dir
  end

  SCRIPT_DIR = File.join(File.expand_path(__dir__), '..')
  CSV_DIR    = File.join(SCRIPT_DIR, 'csv')
  CSV_FILE   = File.join(CSV_DIR, 'processed_emails.csv')
  IMG_DIR    = File.join(SCRIPT_DIR, 'img')
  FONTS_DIR  = File.join(SCRIPT_DIR, 'fonts')
  REPORT_DIR = create_tmp_dir

  def self.run
    begin
      # Fetch emails
      bodies = Reports::IMAP.fetch_unprocessed_emails_bodies

      # Parse bodies
      data = bodies.map do |body|
        Reports::Parser.new(body).parsed_body
      end

      # Generate reports
      files_path = data.map.with_index do |_data, index|
        Reports::Generator.generate(_data, index)
      end

      # Send reports as mail attachments
      files_path.each do |fp|
        Reports::SMTP.send_report(fp)
      end
    rescue NoMethodError => e
      puts "Environment variables may not be loaded. Use `printenv` to verify it or `. </path/to/.env>` to load them"
    end
  end
end

Reports.run
