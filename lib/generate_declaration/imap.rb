# frozen_string_literal: true

module Reports
  module IMAP
    def self.fetch_unprocessed_emails_bodies
      uids             = fetch_emails_uids
      unprocessed_uids = Reports::Tracker.identify_unprocessed_emails_uids(uids)

      begin
        unprocessed_uids.each do |uid|
          Reports::Tracker.save_email_as_processed(uid)
        end

        unprocessed_uids.map do |uid|
          imap.uid_fetch(uid, 'BODY[TEXT]')[0].attr['BODY[TEXT]']
        end
      rescue => e
        puts e.inspect
      end
    end
    
    private

    def self.imap
      begin
        @imap ||= Net::IMAP.new(ENV['IMAP_HOST'], ENV['IMAP_PORT'], false)
        return @imap unless @imap.capability.include?('AUTH=PLAIN')

        if @imap.capability.include?('AUTH=PLAIN')
          @imap.login(ENV['IMAP_USERNAME'], ENV['IMAP_PASSWORD'])
        end

        @imap.examine('INBOX')
      rescue Net::IMAP::NoResponseError
        puts "[-] Wrong credentials"
      ensure
        return @imap
      end
    end

    def self.fetch_emails_uids
      begin
        imap.uid_search([ 'SUBJECT', ENV['IMAP_SUBJECT'] ])
      rescue => e
        puts e.inspect
      end
    end
  end
end

