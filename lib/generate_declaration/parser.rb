# frozen_string_literal: true

# THIS FILE HAS A HUUUUUGE REFACTORING POTENTIAL.

require 'byebug'
require 'nokogiri'

module Reports
  class Parser
    attr_reader :raw_body, :doc, :parsed_body
    def initialize(raw_body)
      @raw_body  ||= raw_body
      @doc       ||= Nokogiri::HTML(raw_body)
      @parsed_body = parse_email_body
    end

    def parse_email_body
      begin
        byebug
      {
        complainant:        complainant_identity,
        firstname:          victime_firstname,
        lastname:           victime_lastname,
        fixed_phone:        victime_fixed_phone_number,
        mobile_phone:       victime_mobile_phone_number,
        email:              victime_email_address,
        date_of_birth:      victime_date_of_birth,
        place_of_birth:     victime_place_of_birth,
        profession:         victime_profession,
        event_date:         event_date,
        event_place:        event_place,
        event_agressor:     event_agressor_identity,
        event_context:      event_context,
        victime_will:       victime_complaint_will,
        contact_procurator: contact_procurator,
        submission_date:    submission_date,
      }
      rescue => e
        puts e.inspect
        byebug
      end
    end

    private

    # Fetch a <div> from the DOM based on a `class` or `id` attribute
    # and returns the corresponding <Nokogiri::XML::NodeSet>
    #   klass:    <String>
    #   id:       <String>
    #-> @returns: <Nokogiri::XML::NodeSet>
    def div(klass: nil, id: nil)
      return @doc.search("//div[@class='#{klass}']") if klass
      return @doc.search("//div[@id='#{id}']")       if id
      nil
    end

    # Fetch a <div> from the DOM based on a `class` or `id` attribute,
    # extract a substring from it based on a Regexp, and returns the
    # corresponding stripped <String>
    #   klass:    <String>
    #   id:       <String>
    #   regex:    <Regexp>
    #-> @returns: <String>
    def div_match(klass: nil, id: nil, regex: nil)
      return if klass.nil? && id.nil?
      return if regex.nil?
      div(klass: klass, id: id).text.match(regex)[1].strip
    end

    def complainant_identity
      div_match(
        klass: 'htmlmail-body',
        regex: /Soumis par l'utilisateur: (.*)/
      ) || 'NA'
    end

    def victime_firstname
      div_match(
        id:    'webform-component-victime--nom',
        regex: /Nom \r\n(.*)/
      ).upcase || 'NA'
    end

    def victime_lastname
      div_match(
        id:    'webform-component-victime--prenom',
        regex: /Prénom \r\n(.*)/
      ).capitalize || 'NA'
    end

    def victime_fixed_phone_number
      div_match(
        id:    'webform-component-victime--ndeg-telephone-fixe',
        regex: /N° téléphone fixe \r\n(.*)/
      ) || 'NA'
    end

    def victime_mobile_phone_number
      div_match(
        id:    'webform-component-victime--ndeg-telephone-portable',
        regex: /N° téléphone portable \r\n(.*)/
      ) || 'NA'
    end

    def victime_email_address
      div_match(
        id:    'webform-component-victime--adresse-mail',
        regex: /Adresse mail \r\n(.*)/
      ) || 'NA'
    end

    def victime_profession
      div_match(
        id:    'webform-component-victime--qualite',
        regex: /Qualité \r\n(.*)/
      ) || 'NA'
    end

    def victime_grade
      div_match(
        id:    'webform-component-victime--grade',
        regex: /Grade \r\n(.*)/
      ) || nil
    end

    def victime_date_of_birth
      div_match(
        id:    'webform-component-victime--date-de-naissance',
        regex: /Date de naissance \r\n(.*)/
      ) || 'NA'
    end

    def victime_place_of_birth
      div_match(
        id:    'webform-component-victime--lieu-de-naissance',
        regex: /Lieu de naissance \r\n(.*)/
      ) || 'NA'
    end

    def victime_number
      div_match(
        id: 'webform-component-victime--nombre-de-personnes-concernees',
        regex: /Nombre de personnes concernées \r\n(.*)/
      )&.to_i || 'NA'
    end

    def event_date
      day = div_match(
        id:    'webform-component-les-faits--date',
        regex: /Date \r\n(.*)/)
      hour = div_match(
        id:    'webform-component-les-faits--heure',
        regex: /Heure \r\n(.*)/)
      [day, hour].join(' à ') || 'NA'
    end

    def event_place
      div_match(
        id:    'webform-component-les-faits--commune',
        regex: /Commune \r\n(.*)/
      ) || 'NA'
    end

    def event_agressor_identity
      div_match(
        id:    'webform-component-les-faits--nom-et-prenom-des-agresseurs',
        regex: /NOM et Prénom de\(s\) agresseur\(s\) \r\n(.*)/
      ) || 'NA'
    end

    def event_context
      div_match(
        id:    'webform-component-compte-rendu-succinct--cr',
        regex: /CR \r\n(.*)/
      ) || 'NA'
    end

    def victime_complaint_will
      will = div_match(
        id:    'webform-component-suivi-des-faits--depot-de-plainte-de-la-victime-sp',
        regex: /Dépôt de plainte de la victime SP \r\n(.*)/
      )

      case will
        when 'Oui'; true
        when 'Non'; false
        when 'Déjà effectué'; victime_complaint_date
        else 'NA';
      end
    end

    def victime_complaint_date
      date = div_match(
        id:    'webform-component-suivi-des-faits--date-du-depot-de-plainte',
        regex: /Date du dépot de plainte \r\n(.*)/
      )

      # Date formating, swap months and days (french format)
      Date.strptime(date, '%m/%d/%Y')
          .strftime('%d/%m/%Y')
    end

    def contact_procurator
      div_match(
        id: 'webform-component-suivi-des-faits--lettre-procureur',
        regex: /Déclaration simplifiée au Procureur \r\n(.*)/
      ) == 'Oui' ? true : false
    end

    def submission_date
      @doc.search('//div[@class="htmlmail-body"]/p[1]')
         .text[/Soumis le (.*)/, 1]
         .gsub('-', 'à').delete(',')
    end
  end
end

