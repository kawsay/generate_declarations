require 'base64'

module Reports
  module SMTP
    # An arbritrary string used as separator between multiple parts of a mail content
    BOUNDARY = '_3c7d2a21904930ec7ff47d0cb268c6605a8d06c02dc50e0c5498926371fae06a68d7'

    def self.send_report(file_path)
      file_content    = File.binread(file_path)
      encoded_content = [file_content].pack('m') # Base64

      mail_content = headers + body + attachment(file_path, encoded_content)

      begin
        # Weak auth due to our 10yo mail server and its too small DH key
        # You're likely to switch to TLS
        # See: 
        #   RFC2554  : https://tools.ietf.org/html/rfc2554
        #   Ruby doc : https://ruby-doc.org/stdlib-2.5.3/libdoc/net/smtp/rdoc/Net/SMTP.html
        smtp = Net::SMTP.new(ENV['SMTP_HOST'], ENV['SMTP_PORT'])

        smtp.start(ENV['SMTP_HELO'], ENV['SMTP_USERNAME'], ENV['SMTP_PASSWORD'], :plain) do |smtp|
          # To add reciptients, simply add arguments to the :send_message method.
          # Each arguments (mail addresses) MUST be separated by a comma as shown below.
          # smtp.send_message(mail_content, ENV['SMTP_FROM'], 'clement.coquille@sdis21.org', 'gso@sdis21.org', 'operation@sdis21.org')
          smtp.send_message(mail_content, ENV['SMTP_FROM'], 'clement.coquille@sdis21.org')
        end
      rescue => e
        puts e.inspect, e.backtrace
      end
    end

    def self.headers
      <<~EOF
        From: script@sdis21.org
        To: cc.lle@pm.me, clement.coquille@sdis21.org
        Subject: =?UTF-8?B?#{Base64.strict_encode64('Déclaration simplifiée de violence')}?=
        Date: #{Time.now.to_s}
        MIME-Version: 1.0
        Content-Type: multipart/mixed; boundary=#{BOUNDARY}
        --#{BOUNDARY}
      EOF
    end

    def self.body
      <<~EOF
        Content-Type: text/html;charset="utf-8"
        Content-Transfer-Encoding:utf8
        Language: fr
        Content-Language: fr

        Ci-joint la déclaration simplifiée de violences commises à l'encontre d'un agent du SDIS.

        Ceci est un mail automatique, ne pas y répondre.
        --#{BOUNDARY}
      EOF
    end

    def self.attachment(file_path, encoded_content)
      file_name = file_path.split('/').last
      <<~EOF
        Content-Type: application/pdf; name="#{file_name}"
        Content-Transfer-Encoding:base64
        Content-Disposition: attachment; filename=#{file_name}

        #{encoded_content}
        --#{BOUNDARY}--
      EOF
    end
  end
end
