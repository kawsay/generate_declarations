# frozen_string_literal: true

module Reports
  module Tracker
    def self.identify_unprocessed_emails_uids(uids)
      begin
        csv = CSV.readlines(CSV_FILE)
                 .map(&:first)
        uids.reject { |uid| csv.include?(uid.to_s) }
      rescue => e
        puts e.inspect
      end
    end

    def self.save_email_as_processed(uid)
      begin
        date = DateTime.now.strftime('%d%m%y_%H%M%S%L')
        CSV.open(CSV_FILE, 'ab') do |csv|
          csv << [uid, date]
        end
      rescue => e
        puts e.inspect
      end
    end
  end
end
