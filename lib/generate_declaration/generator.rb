# frozen_string_literal: true

module Reports
  module Generator

    def self.generate(data, index)
      byebug
      timestamp = DateTime.now.strftime('%d%m%y_%H%M%S%L')
      filename  = "declaration_#{timestamp}.pdf"
      file_path = File.join(REPORT_DIR, filename)

      Prawn::Fonts::AFM.hide_m17n_warning = true

      Prawn::Document.generate(file_path) do |pdf|
        pdf.font_families.update(
          'Times New Roman' => {
            normal: File.join(SCRIPT_DIR, 'fonts', 'times.ttf'),
            bold:   File.join(SCRIPT_DIR, 'fonts', 'timesbd.ttf'),
            italic: File.join(SCRIPT_DIR, 'fonts', 'timesi.ttf')
          }
        )
        pdf.font 'Times New Roman'

        pdf.image(
          File.join(SCRIPT_DIR, 'img', 'logo.jpg'),
          position:  :center,
          vposition: :top,
          width:     80,
          height:    80
        )

        pdf.text_box(
          "COUR D'APPEL DE DIJON
          TRIBUNAL JUDICIAIRE DE DIJON
          Le Procureur de la République",
          at:      [0, 650],
          width:   200,
          leading: 3
        )

        pdf.text_box(
          "<u>Déclaration simplifiée de violences commises à l'encontre d'un agent du SDIS
          À adresser à  <color rgb='#0000ff'>sec.pr.tj-dijon@justice.fr</color></u>",
          at:      [30, 570],
          align:  :center,
          leading: 1,
          inline_format: true
        )

        pdf.text_box(
          "Identité du télé-déclarant: 
          #{data[:complainant]}",
          at:    [300, 500],
          align: :right
        )

        pdf.indent(30) do
          pdf.text_box(
            "Nom: <b>#{data[:lastname]}</b>
            Prenom: <b>#{data[:firstname]}</b>
            N° téléphone fixe: <b>#{data[:fixed_phone]}</b>
            N° téléphone portable: <b>#{data[:mobile_phone]}</b>
            Date et lieu de naissance: <b>#{data[:date_of_birth]} à #{data[:place_of_birth]}</b>
            Profession: <b>#{data[:profession]}</b>
            Adresse mail: <b>#{data[:email]}</b>

            #{Prawn::Text::NBSP * 10}Déclare vouloir dénoncer les faits de violences volontaires (notamment physiques et/ou verbales) dont j'ai été victime, en raison de mes fonctions, sur mon lieu de travail ou aux abords immédiats de ce dernier ou sur un lieu d'intervention.

            Le: <b>#{data[:event_date]}</b>
            À (commune): <b>#{data[:event_place]}</b>
            De la part de (identité et coordonnées si connues): <b>#{data[:event_agressor]}</b>
            Dans le contexte suivant: 
            #{Prawn::Text::NBSP * 10 + '<i>' + data[:event_context] + '</i>'}


            Je déclare vouloir:
              #{complaint_will(data[:victime_will])}


            #{Prawn::Text::NBSP * 10}Je suis informé·e que ce document sera communiqué à un service de police judiciaire chargé de diligenter une enquête et de recueillir mes déclarations, et à l'association d'aide aux victimes (France victimes 21) qui prendra attache avec moi pour m'apporter soutien et conseils.",
            at: [15, 480],
            inline_format: true
          )
        end

        pdf.text_box(
          "Fait le: #{data[:submission_date]}",
          at: [300, 75]
        )
      end

      return file_path
    end

    private

    def self.complaint_will(will)
      if will
        "►  déposer plainte en mon nom propre"
      else
        "►  seulement dénoncer les faits, en me réservant le droit de déposer plainte ultérieurement"    
      end
    end
  end
end

