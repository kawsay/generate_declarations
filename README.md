# generate_declaration
`generate_declaration` est un script ayant pour but de collecter les soumissions d'un formulaire (LimeSurvey) afin d'en générer une synthèse en PDF.


Il est composé de 5 modules détaillés ci-dessous:

- `Reports::IMAP`
- `Reports::Tracker`
- `Reports::Parser`
- `Reports::Generator`
- `Reports::SMTP`

